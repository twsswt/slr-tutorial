# SICSA PhD Conference 2018

## Conducting Systematic Literature Reviews

Tim Storer and Jeremy Singer present an interactive workshop on 
systematic lit reviews in Computer Science, as part of the 
[SICSA PhD Conference](http://sicsaconf.org) at RGU, 28 June 2018.

After a short slide presentation outlining the techniques advocated by 
Kitchenham, we will conduct a systematic literature review
group exercise on *Flipped Classrooms for CompSci*.


