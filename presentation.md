% Conducting Systematic Literature Reviews.<br/>  A Tutorial and Health Warning
% Tim Storer,<br/> School of Computing Science,<br/> University of Glasgow
% SICSA PhD Conference,<br/> Robert Gordon University,<br/> June  2018

---
header-includes:     "<link rel='stylesheet' type='text/css' href='custom.css' />"

---

# Task Overview: Conduct a systematic literature review of  *Flipped Classroom Teaching in Computer Science Education*

> "A flipped classroom is one where students are introduced to content at home, and practice working through it at school.
> Students watch pre-recorded videos at home, then come to school to do the homework armed with questions and at least
> some background knowledge."

[https://www.teachthought.com/learning/the-definition-of-the-flipped-classroom/](https://www.teachthought.com/learning/the-definition-of-the-flipped-classroom/)

---

# Self Organise into Review Teams of ~ 5

(an odd number is better)

 * Appoint someone for data entry (needs laptop with StArt installed)

    http://lapes.dc.ufscar.br/tools/start_tool

 * Others form review pairs (must reach consensus on review process)

---

# Open the StArt Tool

 * Create a new literature review
 * Enter a title and your details as authors, click next
 * Enter a description, click finish

---

# Decide on Research Question(s)

Some examples

* "What is the efficacy of flipped classroom as a teaching  method in Computing Science education?"
* "In what sub disciplines of Computing Science Education has flipped classroom been employed?"
* "Is flipped classroom more effective in Software Engineering or Theoretical Computing Science classes?"

In StArt, edit the protocol

 * Enter an objective and main research question
 * Set the source selection criteria to peer reviewed paper database
 * Set the study language to be English
 * Add ACM to the search list

---

# Decide on Search Terms

 * Conduct a preliminary search of your favourite paper databases (Citeseer, ArXiv.org, IEEEXplore, )
 * Discover relevant related terms
 * Remember that different terms may be used for the same thing, or for unrelated work
 * Examples: 
   * Flipped learning
   * Design studio
   * Blended learning
   * Project based learning
   * MOOC

 * Enter  the search terms into keywords and synonyms in StArt

---

# Decide on Inclusion or Exclusion Criteria

Examples:

 * Top 50 search results (for the sake of brevity in this tutorial!)
 * Number of citations
 * Date ranges (after 1953, for example)
 * University level teaching
 * Computing science focus
 * Scope
 * Study type
 * ...

Add whatever criteria you pick to StArt.

---

# Decide on Quality Standards

Examples:

 * Research questions included, or objectives, or hypothesis?
 * Evaluation included? form?
 * Internal and external threats to validity or limitations identified?
 * ... 

Make sure you save your protocol (.start file) and complete all mandatory fields.

# Decide what fields to Extract

Add these to StArt, under 'Data Extraction Form Fields'

Examples:

 * Number of participants
 * Number of courses in the trial
 * Methodology (e.g. RCT., case study, experience report)
 * Study duration
 * CS Sub-discipline
 * Metric for improvement
 * Conclusions (flipped classroom improvement?)
 * ...

---

# Perform your Search

 * Choose 'New Search Session' from the Review menu and then enter one of your search strings. Click OK
 * Go to 'SEARCH0' under the ACM drop down in StArt
 * Search the ACM Digital Library website and save the first page of results as a bibtex file
 * Import the BibTex file into Start
 * Follow the instructions for removing duplicates

---

# Apply Inclusion, Exclusion and Data Extraction and Quality Check in StArt

 * In selection, right click an entry and choose an inclusion or exclusion reason.
 * In 'Selection' double click a paper and choose the Data Extraction Form tab.
 * Enter the data about the paper and decide whether to accept or reject at this stage.
 * Choose the Quality Form and apply the questions asked after reading the paper.

---

# Review Data Presentation

 * Word Cloud
 * Pie charts
 * Reference analysis

---

# Tips

 * Be prepared to change everything about your SLR protocol as you better understand the field.
   * Weaken quality considerations
   * Widen research questions
   * Expand search terms
 * But document the changes you make.
 * Use version control (Mercurial, Git, Subversion, Fossil...) to manage changes to protocol and results.

 Ask yourself: why are you conducting the SLR?

---

# Health Warning

 * Conducting a systematic literature search of Computing Science literature is fine.
 * Conducting a *meta-analysis* of recovered papers is *very risky*
 * Computing science is a very diverse field
   * Many sub disciplines with wide range of research methods
   * No culture of repeat study
   * No culture of repeat method
 * Wishing the world was otherwise is fine. *Pretending that it is, is not.*

---

# Bad Smells in Computing Science SLRs

* Massive over-specialisation in focus
* Research questions that cannot be realistically answered through a literature review
* Arbitrary quality assessment, sometimes with an unjustified points based system
* Bar charts of paper counts by research method type
* Bar chart showing that the number of papers in topic X is increasing over last n years
* Meta-analysis of study designs, e.g. number of participants
* Calls for more research of type X in subject Y
* ...


