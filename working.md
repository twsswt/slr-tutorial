# Systematic Literature Review Considered Harmful

## Bad Smells in Computing Science SLRs 

* A diagram illustrating the procedure followed for conducing the SLR 
* Massive over-specialisation in focus
* Research questions that cannot be realistically answered through a literature review
* Highly synthetic, indirect, uninteresting, or unanswerable research questions
* Using occurence (or worse, frequency) of some feature (e.g. use of a term) in the literature as evidence of occurence or frequency in the real world.
* Arbitrary selection of inclusion criteria, such as dates considered, acceptable research methods, number of participants
* Arbitrary quality assessment, sometimes with an unjustified points based system.
* Bar chart showing that the number of papers in topic X is increasing over last n years
* Bar charts of paper counts by research method type
* Meta-analysis of study designs, e.g. number of participants
* Calls for more research of type X in subject Y
* Treatment of wildly different artifacts as equilvent atomic units (workshop paper versus article collection, invited paper or PhD thesis) and therefore given equal weight.
* Assuming that all artifacts in a sample are distinct.
* Missing list of excluded papers
* Complete and utter absence of any informative critical assessment of the material gathered.
* Weasle words in conclusions: may, usually...

## Snippets

> "There is a growing interest and literature demands more empirical study to understand the use of Scrum practices in globally distributed projects."
>
> Hossain et al. *Using Scrum in Global Software Development: A Systematic Literature Review*



> "RQ1: Which are the most **common** financial terms used in the
> context of technical debt management?"
>
> Ampatzoglou  et al. *The financial aspect of managing technical debt: A systematic literature review*

> "We chose the start date as July
> 1997 because the first paper on AOP (i.e., Kiczales et al. [1]) ap-
> peared in ECOOP’97."
>
> Ali et al. A systematic review of comparative evidence of aspect-oriented programming

> "Objective: This paper evaluates claims from software engineers and scientific software developers about 12
>  different software engineering practices and their use in developing scientific software."
>
> Heaton and Carver *Claims about the use of software engineering practices in science: {A} systematic literature review*

## Themes

Presumed assumption of ‘gold standard’ research methods (typically a randomised controlled laboratory study).

Desire to perform a meta-analysis (because the book or my supervisor told me to)

## Pseudo Rigour in Science

The mechanical (and rigorous) application of procedures without a consideration as to their appropriateness or the meaningfulness of results.  

## Observations

Systematic literature reviews (comprising systematised searches and analysis) are sensible in fields where the research method and reporting mechanism is largely uniform. For example, in medicine, where the randomised controlled study is a standard and sensible experimental design to discover whether treatment X is effective for illness Y. A systematic *search* is still useful in fields where the research method and reporting mechanism is much more diverse.  However, attempting to perform a meta-analysis and derive conclusions from the results of this are pretty much meaningless.

*The development of guidelines for the mechanical production of systematic literature reviews has allowed researchers to abdicate their responsibility for critical analysis of the material they gather.* The publication of a meta-analysis only type systematic literature review suggest that the author hasn't actually tried to understand (or in some cases even properly read) the research they have gathered.  This is because they neither show any evidence of understanding or being able to synthesis the material, nor that the superficial comparisions that they do make are utterly meaningless.
